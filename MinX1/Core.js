
//Here I declare all the variables I need throughout the code
var gamestate; // 0 = Main menu, 1 = bubbledrawer, 2 = hello there popper
var spawnrate = 2;
var bubbledraw;
let cnv;
let button;
let sel;

//this is the setup run on start
function setup() {
//Making sure we start in game state 0
gamestate = 0;
  frameRate(spawnrate);
    cnv = createCanvas(1920,1080);
//button code to change scene
button = createButton("choosescene");
button.position(50,50);
button.mousePressed(ChangeGameState);
//selector code
sel = createSelect();
sel.position(50,10);
sel.option(1);
sel.option(2);
sel.selected(1);
    // put setup code here
  print("fuckme");
bubbledraw = new Bubble();




}

// this function changes the gamestate/scene and removes all UI elements
function ChangeGameState()
{
  let chosenGameState = sel.value();
    gamestate = chosenGameState;
    removeElements();
}
//Update
function draw() {
//Game state 1 = bubbledraw
if(gamestate == 1){
  frameRate(spawnrate);
//creates a bubble in a random location with a random colour
let c = color(random([0],[255]),random([0],[255]),random([0],[255]));
var bubbledraw = new Bubble(random([1],[1000]),random([1],[1000]),random([20],[50]),random([20],[50]),true);
fill(c);
ellipse(bubbledraw.xPos,bubbledraw.yPos,bubbledraw.heightAndWidth,bubbledraw.heightAndWidth);
// increasing the spawnrate everytime the draw is run (each frame)
spawnrate++
}
// Hello there spawner AKA the remnants of me trying to add interactivity to the bubbledrawer
if(gamestate == 2)
{
  Elip = createImg("https://media.giphy.com/media/Nx0rz3jtxtEre/giphy.gif")
  Elip.position(random(5,1000),random(5,1000))
  Elip.mousePressed(RemoveObiwan);

}
}
//does what it says on the tin, removes obiwan from the canvas and plays hello there
function RemoveObiwan()
{
    this.remove();
    let kenobi;
    kenobi = createAudio("../Assets/obi-wan-hello-there.mp3");
    kenobi.play(true);
}


//tried to create a class and use it, turned out to be more complicated than first anticipated.
class Bubble {
  constructor(xPos, yPos, heightAndWidth) {
    this.xPos = xPos;
    this.heightAndWidth = heightAndWidth;
    this.yPos = yPos;

  }

}
