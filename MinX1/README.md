# MiniX1: Bubbles and Kenobi
<img src="https://i.gyazo.com/4c549ba672fc9c74442a31c0c79d3e9b.mp4" width="480" height="240"/>

**TL:DR** Created a piece of "art" with bubbles and an interactive shitpost of Kenobi, became more profiencient in javascript and its interaction with HTML, and we dont need to learn code but we should be introduced to it.

For this first miniX the goal was to explore and understand how to read and use the reference guide that exists for the p5.js library, I decided to start out easy as this was my first time with javascript and coming from mainly working with c# in unity, my first time not completely bound by the limitations of object oriented coding. Therefore I decided to make a game where you popped bubbles that appeared at a random place with a random color, this, as is evident in the final product didnt happen, but kind of did.

## Links
[The product](https://vongnome.gitlab.io/personalgitlab/MinX1/)

[The Code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX1/Core.js)

## break 1: trying to use a class
The first thing I tried as can be seen as remnants in the code, was to use a class as I needed to save the individual coordinates of each bubble, so that I could later use the coordinates to check whether the mouse was over them before they would be deleted. This did not work, as what the code was essentially doing was drawing a background which, while beautiful was not entirely interactive and I decided that in order to make it work I would have to create my own class containing a all the methods I would need. I figured this out partially by looking at other examples of games on the internet, and what I found was that in order to do something which was simple in c# I would have to create everything from the bottom up, this was too much work so I looked for a solution and found Create.img().

## Break 2: Create.img() and the arrival of Kenobi
Create.img() is  part of the p5.js librarys methods for interfacing directly between javascript and HTML through the p5 element, and DOM (document Object model) object, which is in short is a class that javascript inherits from HTML that contains all "relevant" information for working within the document. The important thing here is that through the use of ellipse I had been drawing on the background canvas, where with the use of Create.img I was adding individual elements **on top of** the canvas. This + the inherited mousepressed() method made it exactly what I was looking. Only problem was that the p5 elements needed to have images to load in, and here I could have gone and found a bubble (or made one), found a way to change its color, instead I chose the easy option and used a picture I already had on hand (the gif of General Kenobi going "hello there") and it stuck.

## Break 3: acceptance
After accepting that I wasnt going make a bubble popping simulator, I parted the game up into two: bubbledraw the noninteractive "art" and the hello there popper, an interactive shitpost. I did this using an if statement that based on the variable "gamestate" sent the player to either scene. To finish it up I added some UI elements (Slider and a button) to make the player able to pick and choose which scene to enter and to add a little bit of choice in my program.

[//]: # "For the sake of my sanity, if anyone is reading this I find it kind of annoying that there are two places giving information about our miniX assignments, and that they are not the same... either only have one place, or keep it the same goshdarnit."

## reflection/the other stuff

1. **what are the differences between reading other people code and writing your own code. What can you learn from reading other works?**
    1. In my humble opinion, reading others'  ~~and stealing~~ code is one of the main ways of learning how to code, as it gives you (the reader) an insight into how other have solved a problem you might be facing. With that said just because you can read it doesnt mean you can write it, "we can all read a book" but we cant all write one, this difference in creative ability isnt something inherent to coding, but all crafts. 
42. **what's the similarities and differences between writing and coding?**
    1. Culture, people, medium, where does the list stop? the two crafts have some similarities in how we learn them best. Coding is learned through the trial and error (or the building of knowledge in action as Schön would put it) of writing some code, seeing the computer spit it out as an error code and then trying again. Writing is similar but instead of a computer its another person that spits out your story in the form of "constructive critiscism". I am here talking about creative writing only, as writing in general is taught in the way some programming is taught unsuccesfully. General writing skill is needed in order to learn everything* (with some exceptions I dont know).
69. **What is programming and why you need to read and write code (a question relates to coding literacy - check Annette Vee's reading)?**
    1. In short *You dont need it* but you probably *should try it* In the text *"coding for everyone"* Vee analyses a literacy campaign to get black girls out there and coding. She breaks this campaign down into a set of arguments for why the girls should learn how to code. I bring this example up because while I agree with the argument she analyses as "individual empowerment" I disagree with most of the others. In todays digital enviroment programming or code knowledge isnt needed to survive or even thrive in, but a general "tech literacy" is, this is reflected by a lot of the sources and qoutes being from an era where arguably there was a need to understand programming to something as simple as moving a file. But times have changed and the rise of GUI and overly designed interfaces has arrived and its here to stay. So when I read old arguments for learning programming, I read it as a need for tech literacy, which there still is, rather than a need for everyone to learnt the ins and out a javascript. with that said I do agree with the argument that every should have an introduction to programming, because as more things are becoming digital, having the ability to create your own apps and the like is definetly freeing and we should (as a society) make sure that everyone can make an educated choice to not code. As such programming should be in the same camp as woodworking, or sowing is in our school system. 

This got a little long and rambly, TODO: make shorter next time

# Sources

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
p5.js. p5.js | get started. Available at: https://p5js.org/get-started/
Lauren McCarthy, Learning While making P5.JS, OPENVIS Conference (2015).
[Daniel Shiffman, Code! Programming with p5.js, The Coding Train.](https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2).
Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93.
