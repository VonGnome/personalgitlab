let ctracker;
let capture;
let mic;
let kisser = false;
let kiss;
function setup()
{
  createCanvas(windowWidth, windowHeight);
    //web cam capture
    capture = createCapture(VIDEO);
    capture.size(640, 480);
    capture.hide();
    //setup face tracker
    ctracker = new clm.tracker();
    ctracker.init(pModel);
    ctracker.start(capture.elt);
    mic = new p5.AudioIn();
    mic.start();
}



function draw() {
  //draw the captured video on a screen with the image filter

    image(capture, 0,0, 640, 480);
    //filter(INVERT);
    let vol = (map(mic.getLevel(),0,1,50,470));
    let positions = ctracker.getCurrentPosition();

    ellipse(50,vol,40,40);


    //check the availability of web cam tracking
    if (positions.length) {
       //point 60 is the mouth area
    //button.position(positions[60][0]-20, positions[60][1]);

      /*loop through all major points of a face
      (see: https://www.auduno.com/clmtrackr/docs/reference.html)*/
      beginShape()
      for (let i = 44; i < 62; i++)
      {
      vertex(positions[i][0],positions[i][1]);
      }
      endShape()

      for (let i = 44; i < 62; i++) {
         noStroke();
         //color with alpha value
         fill(map(positions[i][0], 0, width, 100, 255), 0, 0, 120);
         //draw ellipse at each position point
         ellipse(positions[i][0], positions[i][1], 5, 5);
      }


    }
    if(kisser == true){
      //console.log("KISSME");
      //let savePos = posit
      //console.log(savePos);
      //beginShape()
      //for (let i = 44; i < 62; i++)
      //{
      //vertex(savePos[i][0],savePos[i][1]);
      //}
      //endShape()

      //Here lies the text version
      //rectMode(CENTER);
      //textAlign(CENTER);
      //rotate(10);
      //textSize(50);
      //text("💋",positions[60][0],positions[60][1]);

    }

}

function keyPressed()
{
    if(keyCode == 69)
    {
      kisser = true;
    }
}
