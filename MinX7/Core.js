//the one variable for putting bubbles in
let bubbleList = []

function setup()
{
  frameRate(10);
  createCanvas(windowWidth, windowHeight);

}


function draw() {
  //for loop for changing the color and removal of bubbles
  for (var i = 0; i < bubbleList.length; i++) {
    if(bubbleList[i].Pop(mouseX,mouseY))
    {
      bubbleList[i].ChangeColor();
      //bubbleList.splice(i, 1);

    }
  }
  //Creation and drawing of bubbles
  bubbleList.push(new Bubbles());
  DrawBubble();
}
//Funcion for showing bubbless
function DrawBubble(){
  for (var i = 0; i < bubbleList.length; i++) {
    bubbleList[i].Show();


  }
}
