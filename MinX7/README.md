# MiniX7: burnout
[![Image from Gyazo](https://i.gyazo.com/12cc277e4f7059f673bd7336398526af.png)](https://gyazo.com/12cc277e4f7059f673bd7336398526af)
## the tired links
[The product](https://vongnome.gitlab.io/personalgitlab/MinX7/)

[the code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX7/Core.js)

## the thing
This week the task was to create a game, and if you look at my previous miniX' I have created small games for a lot of them (I point especially to minix4 where I made flappy bird) and this time.... this time I have created a small bubble popping "game" with the goal to create the least stressful experience imaginable, which one might ask how is that critical weeeelll...

## Burnout, games and crunch culture
This is the 7th miniX and we are roughly halfway through the semester, and after spending many hours on weekly assignments all blending into a sea of mediocre productions I have become burned out. This is especially due to my own and many other students tendency for working in the weekends due to the monday lectures and the supposed amount of "free time" to work in its very tempting to skip abit of free time to create something truly amazing for the weekly assignment. In other words, we students have a tendency to crunch. And while crunch for the sake of passion is a selfdestructive way of working, it doesnt lead to the same horrifying effects as the stress of the crunch culture that is all to prevalent in the video games industry. The important difference between the two is that crunch coming down from on high, is always non negotiable and often something done for months in order to meat deadlines that comes from the gamer(tm) community. Because to the ordinary gamer(tm) that wants their hot new game, the game developer is nothing more than an object abstracted by the class of companies, hidden behind the good pr of companies trying to keep their profits as high as inhumanly possible,

### A personal note

As previously mentioned this Week I was feeling some serious burnout, and I mentioned in our mid semester evaluation that, the motivation for weeks where we dont get feedback was lower than the weeks where we got it. here I chose the wrong word, as what I should have said the desire and motivation to *crunch* is lower, yet the workload is the same, and the difference between this and a "pause week" is indistinguashable as the workload and expectations for my self is set as high as possible.

# Sources
Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on blackboard\Literature)
[“p5.js examples - Objects,”](https://p5js.org/examples/objects-objects.html).
[“p5.js examples - Array of Objects,”](https://p5js.org/examples/objects-array-of-objects.html).
[Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train (watched: 2.3, 6.1, 6.2,6.3, 7.1, 7.2, 7.3)](https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA).

