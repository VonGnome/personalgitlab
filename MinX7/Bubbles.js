class Bubbles {
  constructor() {
    this.position = createVector(random(width - 100), random(height - 100)); //random position
    this.color = color(random(255),random(255),random(255)); //color
    this.size = floor(random(10,50)); //size in radius
    this.EasyWayOut = 0; // cheap way of getting the bubbles removed
    this.audioPicker = floor(random(1,4)); //random variable for picking audio
    //instead of loading all three we only load 1
    this.bubbleSound1 = "../Assets/BubblePop1.wav";
    this.bubbleSound2 = "../Assets/Bubblepop2.wav";
    this.bubbleSound3 = "../Assets/BubblePop3.wav";

  }
  //method for showing/drawing the bubble
  Show(){
    fill(this.color);
    stroke(this.EasyWayOut);
    ellipse(this.position.x,this.position.y,this.size * 2);
  }
  //the P stands for Pop x and Pop y not pixel who would ever do that...
  Pop(px,py){
      let d = dist(px,py,this.position.x,this.position.y)
    if(d < this.size)
    {

      return true;

    } else return false;

  }
  //This method "removes" the bubble using rather than splice because I have given up
  ChangeColor(){
    let pickedAudio;
    if(this.audioPicker == 1) {
      pickedAudio = createAudio(this.bubbleSound1);
    }
    if(this.audioPicker == 2) {
      pickedAudio = createAudio(this.bubbleSound2);
    }
    if(this.audioPicker == 3) {
      pickedAudio = createAudio(this.bubbleSound3);
    }
    pickedAudio.play(true);
    this.EasyWayOut = 255;
    this.color = color(255,255,255);

  }


}
