# MiniX6: importing some help for password creation
[![Image from Gyazo](https://i.gyazo.com/ac8bebae5ef861d7cb20ecf1a09e5a51.png)](https://gyazo.com/ac8bebae5ef861d7cb20ecf1a09e5a51)
## the l1nk5
[The product](https://vongnome.gitlab.io/personalgitlab/MinX6/)

[the code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX6/Core.js)

[miniX5](https://gitlab.com/VonGnome/personalgitlab/-/tree/master/MinX5)

## Side note:
This program uses the free version of the [Random.org](https://api.random.org/json-rpc/2) which means it can "only" be used 1000 times a day, so if for some reason it doesnt work this might be the reason also NO STEALING THE API KEY PLEASE, if the program doesnt work and the api key is gone, then blame the people who stole the key.

Also the program seems to have trouble running on linux systems, it runs fine on windows and mac, so try that if it doesnt work.

## What has been changed from last time
Im not gonna describe the program again, as I have already done that in my miniX5 so if you wanna know how it works go read that (link above). What I have changed is small yet very significant. the programs goal is trying to critique/bring to light the ways bots are generally better at creating passwords than us, by framing it as a race. But in a sense I created a "fake", because the password bot used the random function in javascript, which uses pseudorandomness causing the entire race to be a fake because if the guesserbot was just a bit smarter (or real) it wouldnt be better than a human. In the new version the old makeID bot is dead, and has been replaced by the RANDOM.orgs true random. This ofcourse has the effect of making the program not able to be as generative as the original (because I am not calling the api over and over and...) 

## Did I do "a critical making"?
I am choosing to pose this as a question for you, oh humble feedback giver and you oh great teacher/censor, because I dont think you truly can. Because in order to answer the question, we must ask "when" is something critical making?, we can say when a product is critical by looking at its messaging and by analyzing the ways it critiques the chosen subject matter, and we can do the same with our ideas. Taking this miniX as an example, the changes made to the program are small but change how it works in a major way, and makes it "true" to its intentions, which we could argue makes it better at getting its message across to its audience, but **while implementing** the api did I think about the larger consequences of using it rather than the function, and can you the reader see it? If critical making lies in the process of creation, then how do we find it? my argument is that it lies in our intentions, and what we as programmers artists and or designers want out of our product. And if thats the case you can never truly know my intentions, which means you cant tell if I have done a critical making. You can assume I have done it, based on the fact that we are in an aesthetic programming class where we are being taught how to think along these lines, and because its part of the assignment to do it, but if you try and find out *why* my thesis is that if you say **No there is no critical making here** it would be wrong, as only I know my true intentions, so if I say I did, then I did. While I argue this, I would be happy to find a way to see critical making, as in this current narrow definition, I think it will be hard to implement outside of artworks made for the express purpose of being critical, as any deviancy from the purest of intentions means I lose the critical making status, which would be sad because being aware of the implicit effects our code can have on the world seems like an important tool for design.

# Sources
* Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
* [Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28](https://networkcultures.org/blog/publication/the-critical-makers-reader-unlearning-technology/)
* [Daniel Shiffman, “Working with data - p5.js Tutorial,” The Coding Train (10.1, 10.4 - 10.10)](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r)
