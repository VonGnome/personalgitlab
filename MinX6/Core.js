//variable declaration
let playerInput
let passwordLength;
let playerPassword;
let playerPasswordText;
let botPasswordText
let playerPasswordTextGuesser;
let botPasswordTextGuesser
let url = "https://api.random.org/json-rpc/2/invoke";
let startButton;
let guessedBotPassword;
let guessedBPlayerPassword;
let raceStart = false;
let pointer;
let trueRandom;
let passwordCracked = false;
let botpassword2;
let postData;
let botpassword;
let description;
let truerandomPasswordBotsbrain = [];
//preload for defining postdata, a JSON object to POST to the random.org api
function preload(){
    postData = {"jsonrpc": "2.0",
    "method": "generateUUIDs",
    "params": {
        "apiKey": "867e78f7-bec3-4f8a-80a9-471ea3829534",
        "n": 1
    },
    "id": 15998}


}

function setup()
{
  // This is where we call the api by posting to it
  httpPost("https://api.random.org/json-rpc/2/invoke",'json',postData, function(result){

    for (var i = 0; i < result.result.random.data.length; i++) {
      truerandomPasswordBotsbrain.push(result.result.random.data[i])
    }
    botpassword = truerandomPasswordBotsbrain[0];

  })
createCanvas(windowWidth,windowHeight);
frameRate(15);
  //Creation of the input DOM element
  playerInput = createInput("Shittypassword","password")
  playerInput.position( width / 2, height / 2)
  playerInput.input(OnInput);
  //Creation of the description text
  description = createDiv("Try and write a Password that can beat the bots, click race when you have written a password");
  description.style('font-size', 25 + 'px')
  description.position((width / 2 ) - 400, (height / 2) - 100)

  //Creation of the playerPasswordtext
  playerPasswordText = createDiv("");
  playerPasswordText.position((width / 2 ) -400, (height / 2) + 50)
  playerPasswordTextGuesser = createDiv("The hacker bot guessed the password:");
  playerPasswordTextGuesser.position((width / 2 ) -400, (height / 2) + 120)

  //Creation of the botpasswordText
  botPasswordText = createDiv("");
  botPasswordTextGuesser = createDiv("The hacker bot guessed the password:");
  botPasswordText.position( (width / 2 ) + 50, (height / 2) + 50 );
  botPasswordTextGuesser.position( (width / 2 ) + 20, (height / 2) + 70 );

  //Creation of the startbutton
  startButton = createButton("Race!");
  startButton.position(width / 2, (height / 2) - 60);
  startButton.mousePressed(StartRace);


}



function draw() {
  background(75);
  //Variables to hold the guessed passwords by the guesserBot
let guessedPlayerPassword
let guessedBotPassword
//raceStart changed in the StartRace function
if(raceStart == true) {
  guessedPlayerPassword = GuesserBot(playerPassword);
  guessedBotPassword  = GuesserBot(botpassword);
}

//Text writing
playerPasswordText.html("the players password is:" + playerPassword);
playerPasswordTextGuesser.html(guessedPlayerPassword)
botPasswordTextGuesser.html(guessedBotPassword);
botPasswordText.html("The Bots password is:" + botpassword);
}

//here lies the corpse of the old password bot. long live random.orgs random generator
function makeid(length) {
   var botPassword = '';
   var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      botPassword += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return botPassword;
}
//function called on button click
function StartRace(){
raceStart = true;
}

//Oninput to set the playerpassword so the GuesserBot can read it (stupid thing can only reaad strings)
function OnInput()
{
  playerPassword = this.value();
  passwordLength = playerPassword.length;

}
//GuesserBot, the "bot" that through a selectionsort-ish guesses the contents of a string value
function GuesserBot(password){

  let interations = 0; //Iterations variable for saving how long it takes to guess
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-'; // the bots alhpabetic knowledge
  var arr = Array.from(password) //turning the string value into an array for better guessing

  //Variable for holding the bots guess
  let guessedPassword = [];
    //for loop for guessing the entire password
    for(var i = 0; i < arr.length; i++)
    {
      //Checks through characters if array is eqaul to character is the first string in the pass e? if not is it e...
      for(var j = 0; j < characters.length; j++){
      pointer = characters.charAt(j);


      //returns usable guesses
      if ( pointer == arr[i])
      {

        guessedPassword.push(pointer);

      }
      else interations++;

    }

    }
    //Returns the bots guess
    guessedPassword.toString();
    passwordCracked = true;
    return ("The hacker bot guessed the password:" + guessedPassword + " after:" + interations + " iterations");


}
