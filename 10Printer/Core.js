let x = 0;
let y = 0;
let spacing = 10;


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  //spacing = random(100);
  if (random(1) < 0.5) {
    stroke(250,100,0);
    //line(x, y, x+spacing, y+spacing);
    fill(random(250));
    rect(x,y,x+spacing,y+spacing);
  } else {
    stroke('red');
    line(x, y+spacing, x+spacing, y);
  }
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
  if( y > height)
  {
    y = 0;
  }
}
