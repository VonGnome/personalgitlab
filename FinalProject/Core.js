// Title: Make me non-binary
// Final project for the Aesthetic Programming course
// BA in Digital Design
// Group 10
// Members: Asger Mørk Ladekjær, Cecilie Vedsted, Markus Elvig Løfgren and Mathias Krogh Højgård

let entities, theBeauty, adjectives; //Variables for keeping the JSON elements

//empty array for holding the person objects
let majorityReport = [];
let tolerableAmountOfPersons = 4; //Control variable for max amount of person objects

//variables for creating and controlling the timer
let timerStart = 0;
let timerEnd = 3000;   //timer takes 3 seconds before triggering;
let text2Timer = 30000 //30 sec. = 30000 milisec.
let timerStart2 = 0;
let buttonTimer;
let buttonTimerEnd = 5000; //5 seconds
let textTimer = 0;

//Variables for image
let capture;
let img;
let moveSize = 652; //Capture size

//Variables for program control
let cnv;  //Canvas
let showImage = false;
let programStart = false;

//Variables for DOM elements
let binaryButton;
let xOffset, yOffset; //Pixel offsets for the X and Y axes to align DOM elements with canvas
let x, y;             //X and Y coordinates for capture of webcam
let title;
let pridefulFont;     //For "Gilbert" font
let startButton;

//Global variables needed by the person object
let spawnPicker;  //Will be used to choose spawning spot for statement
let lastPicked = 0;
let middle;

//Variables for color changing
let bgColor;
let red = 255;
let green = 252;
let blue = 0;
let colorSwap = 0;  //This variable controls colorswap on canvas

//Variables for text control
let textPos = 0;
let textAnimator = 1; //For pink text size (start size)
let i = 0;
let text2 = ["Snapchat and other apps like it,\nfocus on the perceived gender", "When we only turn the\nvisual into data we forget what\nmatters and the people in between","There is more between the feminine, \nand the masculine","So...", "Please, take this into consideration when\nmeeting new people and remember\nto keep an open heart!🌈❤️"]

function preload(){
  //Loads JSON files for use by the person objects
  entities = loadJSON('Assets/entity.json');
  theBeauty = loadJSON('Assets/generated_within.json');
  adjectives = loadJSON('Assets/encouraging_words.json')

  //Loads Iphone image
  img = createImg('Assets/IphoneFrame.png');

  // loads the Gilbert font from file
  pridefulFont = loadFont('Assets/Gilbert_TypeWithPride.otf')
}

function setup(){
  //Set the offsets before canvas is created
  xOffset = 25;
  yOffset = 35;

  // Creates canvas with a size fitting within the Iphone frame
  cnv = createCanvas(1135, 500);      //So it fits within Iphone frame
  cnv.position(xOffset, yOffset);     //0,0 for the canvas
  x = (width/2) - (moveSize/2);       //X coordinate calculated for capture
  y = (height/2) - (moveSize/2) + 32; //Y coordinate calculated for capture (+ a little tweaking to fit within Iphone frame)

  bgColor = color(red, green, 0);
  textPos = width/2;

  //Create dom interactable elements
  //Start program button on front page
  //Green button styling credited to w3schools.com at this link https://www.w3schools.com/howto/howto_css_animate_buttons.asp
  startButton = createButton("Start Program");
  startButton.position((width/2) - 200 + xOffset, height/2);
  startButton.size(400, 100);
  startButton.style('font-family', 'Gilbert_TypeWithPride');
  startButton.style('font-size', '40px');
  startButton.style('cursor', 'pointer');
  startButton.style('text-align', 'center');
  startButton.style('color', '#fff');
  startButton.style('background-color', '#4CAF50');
  startButton.style('border', 'none');
  startButton.style('border-radius', '15px'); //Rounded corners
  startButton.style('box-shadow', '0 9px #999');
  startButton.mouseOver(changeButtonColor);
  startButton.mouseOut(originalButtonColor);
  startButton.mousePressed(startGame);

  //Button for returning to homepage
  returnButton = createButton("Return to start");
  returnButton.position((width/4) - xOffset, height/1.31);
  returnButton.style('font-family', 'Gilbert_TypeWithPride');
  returnButton.style('font-size', '14px');
  returnButton.style('color', '#fff');
  returnButton.style('height', '130px');
  returnButton.style('width', '130px');
  returnButton.style('border-width', '6px');
  returnButton.style('background-color', '#4CAF50');
  returnButton.style('border', 'none');
  returnButton.style('border-radius', '100%');  //Circular button
  returnButton.style('box-shadow', '0 9px #999');
  returnButton.mousePressed(restart);
  returnButton.mouseOver(changeButtonColor);
  returnButton.mouseOut(originalButtonColor);
  returnButton.hide(); //It's a Surprise Tool That Will Help Us Later

  //Spread the love button
  loveButton = createButton("Spread the love");
  loveButton.position((width/2) - 20 - xOffset, height/1.31);
  loveButton.style('font-family', 'Gilbert_TypeWithPride');
  loveButton.style('font-size', '14px');
  loveButton.style('color', '#fff');
  loveButton.style('height', '130px');
  loveButton.style('width', '130px');
  loveButton.style('border-width', '6px');
  loveButton.style('background-color', '#4CAF50');
  loveButton.style('border', 'none');
  loveButton.style('border-radius', '100%');  //Circular button
  loveButton.style('box-shadow', '0 9px #999');
  loveButton.mousePressed(shareTheLove);
  loveButton.mouseOver(changeButtonColor);
  loveButton.mouseOut(originalButtonColor);
  loveButton.hide();  //It's a Surprise Tool That Will Help Us Later

  //Stay a while button
  stayButton = createButton("Stay here for a while");
  stayButton.position((width/1.5) + 4 +  xOffset, height/1.31);
  stayButton.style('font-family', 'Gilbert_TypeWithPride');
  stayButton.style('font-size', '14px');
  stayButton.style('color', '#fff');
  stayButton.style('height', '130px');
  stayButton.style('width', '130px');
  stayButton.style('border-width', '6px');
  stayButton.style('background-color', '#4CAF50');
  stayButton.style('border', 'none');
  stayButton.style('border-radius', '100%');  //Circular Button
  stayButton.style('box-shadow', '0 9px #999');
  stayButton.mousePressed(removePinkText);
  stayButton.mouseOver(changeButtonColor);
  stayButton.mouseOut(originalButtonColor);
  stayButton.hide();  //It's a Surprise Tool That Will Help Us Later

  //Make me non-binary button
  binaryButton = createButton("Make me non-binary");
  binaryButton.position(width/16, height/2 - yOffset);
  binaryButton.style('font-family', 'Gilbert_TypeWithPride');
  binaryButton.style('font-size', '14px');
  binaryButton.style('color', '#00000');
  binaryButton.style('height', '145px');
  binaryButton.style('width', '145px');
  binaryButton.style('border-style', 'solid');
  binaryButton.style('border-width', '6px');
  binaryButton.style('background-color', 'transparent');  //Made to look like Snapchat's layout
  binaryButton.style('border-color', 'black');
  binaryButton.style('border-radius', '100%');
  binaryButton.mousePressed(removeTheBinary);
  binaryButton.hide();  //It's a Surprise Tool That Will Help Us Later

  title = createDiv("make me non-binary");
  title.position((width/2) - 270, height/5);
  title.style('font-size', '70px');
  title.style('font-style', 'BOLD');
  title.style('font-family', 'Gilbert_TypeWithPride');

  //initates capture element for the capture of camera
  capture = createCapture(VIDEO);
  capture.size(moveSize, moveSize);
  capture.position(x, y);                //Positions capture at calculated coordinate
  img.size(width * 1.05, height * 1.13); //Iphonesize

  capture.hide(); //It's a Surprise Tool That Will Help Us Later
}

function draw(){
  background(bgColor); //setting background here to not draw over the image element

  //If statement that checks if the start button has been pressed, most of the program happens here
  if(programStart == true){

    //Start changing the color of the background
    bgColor = color(red, green, blue);

    //Checks if the non-binary button has been pressed
    if(showImage == true) {

      //Timer for making sure the statements dont overlap one another
      if(millis() - timerStart > timerEnd){
        // Starts the person class appearing
        createPersons();
        timerStart = millis(); //resets the timer
      }

      //shrinks the capture image down towards its center...ish
      if (moveSize >= 1){
        moveSize -= 0.5;
        //When the image shrinks it also moves abit (for some dumb reason), so we offset it here
        //Dont ask why we use these numbers, they are a mystery...
        y += 0.25;
        x += 0.29;
      }

      //Updates the captures position and size
      capture.position(x, y);
      capture.size(moveSize,moveSize);

      //Runs function that changes background colour
      changeColor();
    }

    //Makes the persons move and makes them make statements
    controlPersons();

    //Sets text style and size for statements
    textSize(50);
    textStyle(BOLD);

    //We have another showimage check here because of the text hiearachy
    if(showImage == true){

      // This block of code creates and controls the pink text elements (aka text2) at the end of the program
      if(millis() - timerStart2 > text2Timer){
          let textTimerEnd = 5000;      //textimer wait for 5 secs
          push();
            textAlign(CENTER, CENTER);
            textSize(textAnimator);     //sets the size of the text to be changeable
            fill('pink');               //fills with the pink colour
            text(text2[i], textPos, height / 2); //Gets text from list text2, sets it at textPos and shows it in the middle of the screen
          pop();

          capture.hide(); // hiding the camera so it is no longer visible

          //Makes the text grow in size till its at the middle of the screen
          if(textAnimator != 50){
            textAnimator += 1;
            textTimer = millis();
          }
          //Controls that the text only countsdown once its in the middle of the screen
        if(textAnimator == 50){

          // Counts down a timer for when the text is removed from the screen
          if(millis() - textTimer > textTimerEnd && i + 1 < text2.length){
            textPos += 30; //Moves the text away from screen

            if(textPos > width + 500){
              textTimer = millis();
              buttonTimer = millis();
              i++;                 //moves to the next text element in text2
              textPos = width / 2; //resets the position of the text element from text2
              textAnimator = 1;    //resets the size of the text element from text 2
            }
          }
          //Shows the 3 buttons at the end for increased agency
          if(millis() - buttonTimer > buttonTimerEnd && i + 1 == text2.length){
              loveButton.show();
              returnButton.show();
              stayButton.show();
          }
        }
      }
    }
  }

  img.position(0, 0); //This layers iphone image on top of canvas

  // Text on the startscreen is shown here
  if(programStart == false){
    //text styling
    push();
      textAlign(CENTER, CENTER);
      textSize(25);
      textStyle(ITALIC);
      text("CW: this artwork deals with non-binary and transgender issues", width/2, height/1.3);
    pop();
  }
  //Removes persons from the canvas here
  removePersons();
}

// function for changing the button color
function changeButtonColor(){
  this.style('background-color', '#3E8E41');
}

// function for changing the button colour back to its original colour
function originalButtonColor(){
  this.style('background-color', '#4CAF50');
}

//Changes the color of background dynamically, colours are that of the NB flag
function changeColor(){
  //turns background white
  if(colorSwap  == 0){
    if(red != 255) red += 0.5;
    if(green != 255) green += 0.5;
    if(blue != 255) blue += 0.5;
  }

  if(red >= 255 && green >= 255 && blue >= 255) colorSwap = 1; //goes to next color

    //turns background purple
    if(colorSwap == 1){
      if(red != 156 ) red -= 0.5;
      if(green != 89 ) green -= 0.5;
      if(blue != 209) blue -= 0.5;

      if(red == 156 && green == 89 && blue == 209) colorSwap = 2; //goes to next color
    }

    //turns background black
    if(colorSwap == 2){
      if(red != 0) red -= 0.5;
      if(green != 0) green -= 0.5;
      if(blue != 0) blue -= 0.5;
      if(red <= 0 && green <= 0 && blue <= 0) colorSwap = 3; //goes to next color
    }

    // turns background yellow
    if(colorSwap == 3){
      if(red != 255) red += 0.5;
      if(green != 244) green += 0.5;
      if(blue != 48) blue += 0.5;
    }

    if(red == 255 && green == 244 && blue == 48) colorSwap = 0; //goes to next color
}

//Function for creating and filling the list of person objects
function createPersons(){
  lastPicked = spawnPicker; //Checks position for last picked statement
  middle = createVector(width / 2, height / 2);

  if(majorityReport.length < tolerableAmountOfPersons){
    majorityReport.push (new Person(middle, spawnPicker))
    spawnPicker = floor(random(4));

    if (spawnPicker == lastPicked){
      spawnPicker = floor(random(4));
    }

    for (let i = 0; i < majorityReport.length; i++){
      majorityReport[i].spawn();
    }
  }
}

//Function for removing objects at the bottom of the canvas
function removePersons(){
  for (let i = 0; i < majorityReport.length; i++){
    if(majorityReport[i].speed >= 0.01) {
      majorityReport.splice(i, 1); //Removes the statements when speed is too low
    }
  }
}

//Function for controlling the individual person objects in the forbes list
function controlPersons(){
  for (let i = 0; i < majorityReport.length; i++){
    majorityReport[i].statementMaker();
    majorityReport[i].move();
  }
}

//Creates a snapshot of the video without changing the contents of it
function removeTheBinary(){
  showImage = true;

  capture.pause();                //Pauses the camera
  timerStart = millis();
  timerStart2 = millis();
  spawnPicker = floor(random(4)); //Sets the spawnpicker variable before the program starts so the first person knows where to spawn
  this.hide();                    //hide the "make me non-binary" button
}

//Function run on "start program" button press, starting the program and hiding/showing specific elements
function startGame(){
  programStart = true;
  this.hide();          //hide the "start program" button
  title.hide();         //hide the title element
  binaryButton.show();  //show the "make me non-binary" button
  capture.show();       //show the capture
}

//Refreshes the page
function restart(){
  location.reload(); //HTML magic, reloads the page (wooooo)
  return false;      //needed so the button works, and doesnt try and return a value
}

//Shares the love on twitter
function shareTheLove(){
  window.open('https://twitter.com/intent/tweet?url=https%3A%2F%2Fvongnome.gitlab.io%2Fpersonalgitlab%2FFinalProject%2F');
}

function removePinkText(){
  loveButton.hide();  //hide the "spread the love" button
  stayButton.hide();  //hide the "stay here for a while" button
  returnButton.position(xOffset + 20, yOffset + 10);  //reposition the "return to start" button to upper left corner of iphone
  text2 = [""];       //this empties the text2 array, which results in the pink text dissapearing
}

//the person class, containing all we need to know about the rich
class Person {
  constructor(targetVector, picker){
    this.entities = entities.entities[floor(random(entities.entities.length))];                          //assign random entity from JSON file to this.entities
    this.adjectives = adjectives.encouraging_words[floor(random(adjectives.encouraging_words.length))];  //assign random adjective from JSON file to this.adjectives
    this.theBeauty = theBeauty.within[floor(random(theBeauty.within.length))];                           //assign random beautyWithin from JSON file to this.theBeauty
    this.statement = "";  //Variable containing the persons statement
    this.ending = "";     //Variable for containing the word endings
    this.pos = createVector(random(10, (width - 5)), random(10,height)); //Vector for saving the persons X and Y position
    this.existense; // makes sure the person exists on the canvas
    this.speed = 0;
    this.textSize = 20;
    this.target = targetVector;
    this.picker = picker;
    this.hasSpawned = false;
  }

  //Method for choosing both spawn area and target vector
  spawn(){
    //Checks to see if the person has spawned yet, order to only run this once per person
    if (this.hasSpawned == false){
      //Picks the upper left corner to spawn in and go towards
      if(this.picker == 0){
        this.pos.set(random(0, (width / 2) - 50 ), random(0, (height / 2) - 50));
        this.target.set(this.target.x - 20, this.target.y - 20);
      }

      //Picks the bottom left corner to spawn and go towards
      else if(this.picker == 1){
        this.pos.set(random(0, (width / 2 ) - 50), random((height / 2) + 40, height));
        this.target.set(this.target.x - 20,this.target.y + 20);
      }

      //Picks the top right corner to spawn in and go towards
      else if(this.picker == 2){
        this.pos.set(random((width / 2) + 50, width), random(0, (height / 2) - 50));
        this.target.set(this.target.x + 20,this.target.y + 20);
      }

      //Picks the bottom right corner to spawn and go towards
      else if (this.picker == 3){
        this.pos.set(random((width / 2) + 50, width), random((height / 2) + 50, height)); // this controls the spawning of the person
        this.target.set(this.target.x + 20,this.target.y - 20);
      }

      this.hasSpawned = true;
    }
  }

  move(){
    this.textSize -= 0.02;
    this.speed += 0.00001;

    this.pos.lerp(this.target,this.speed);  // moving rule for the statements, add 2 pixels to y position of the statements every frame
  }

  statementMaker(){
    //This if-statement functions as a grammar-checker. Adds a "n" if there should be a "an" before the adjective
    if (this.adjectives.charAt(this.adjectives[0]) == "a" || this.adjectives.charAt(this.adjectives[0]) == "e" || this.adjectives.charAt(this.adjectives[0]) == "i" || this.adjectives.charAt(this.adjectives[0]) == "o"){
      this.ending = "n";  //add the "n" = "an"
    }

    this.statement = "Forget about the perceived." + "\nCare about " + this.theBeauty + ".\nYou are a" + this.ending + " " + this.adjectives + " " + this.entities + ".";  // fill in the strings for the individual statement

    //text styling
    fill(0, 102, 51);
    textSize(this.textSize);
    this.existense = text(this.statement, this.pos.x, this.pos.y);  //writes the statement to the canvas on method call
  }
}
