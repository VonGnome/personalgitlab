let buttons = document.getElementsByClassName("btn");
let angle;
let currentAngle;
let cursorRadius = 100;
let currentPositionX;
let currentPositionY;
let offset;
let menuStatus = false;
let menu = document.getElementById("radial");
let  bgColor;
window.addEventListener("gamepadconnected", function(e) {
  console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
    e.gamepad.index, e.gamepad.id,
    e.gamepad.buttons.length, e.gamepad.axes.length);
});

//makes a randon color for use in the background
function random_bg_color() {
    var x = Math.floor(Math.random() * 256);
    var y = Math.floor(Math.random() * 256);
    var z = Math.floor(Math.random() * 256);
    bgColor = "rgb(" + x + "," + y + "," + z + ")";
}

//Homemade start
window.onload = function Start(){
  //radius of cursor circle based on the desired base menu of 4 buttons
  cursorRadius = (buttons.length / 4) * 150;
  document.getElementById("radial").style.width = (cursorRadius * 2) + "px";
  document.getElementById("radial").style.height = (cursorRadius * 2) + "px";
  //
  for (var i = 0; i < buttons.length; i++) {
    buttons[i].style.display = "none";
  }
  random_bg_color();
  document.getElementById("radial").style.display = "none";
  document.body.style.background = (bgColor);

}
let pointerArrow = document.getElementById("pointerArrows");
const onMouseMove = (e) => {
  document.getElementById("pointerArrows").setAttributeNS(null, 'x2', e.pageX);
  document.getElementById("pointerArrows").setAttributeNS(null, 'y2', e.pageY);
}
  document.addEventListener("mousemove",onMouseMove)
//The big interaction onclick located on the document
document.onclick = function(e){
  cursorRadius = (buttons.length / 4) * 150;
  //Checks if the menu is opened and if the mouse is targetting a btn
  if(menuStatus == false && e.target.className !="btn"){
    menuStatus = true;
    //Shows the buttons
    for (var i = 0; i < buttons.length; i++) {
      buttons[i].style.display = "block";
    }
    //Shows the the radial menu
    document.getElementById("radial").style.display = "block";
  //re sizes the circle for new buttons
  document.getElementById("radial").style.width = (cursorRadius * 2) + "px";
  document.getElementById("radial").style.height = (cursorRadius * 2) + "px";

  //you might be wondering why we dont use the menu variable here... me too ;(
  document.getElementById("radial").style.left = e.clientX + "px"
  document.getElementById("radial").style.top = e.clientY + "px"
  //Finds the 1% angle/angles between each button in radians (dont ask about the math) Borrowed from https://codepen.io/soybisonte/post/the-math-core-of-a-radial-menu
  angle = 2 * Math.PI / buttons.length; // 360 divided by the number of buttons (standard 4)
  //Creates a more balanced circle (dont ask about the math) Borrowed from https://codepen.io/soybisonte/post/the-math-core-of-a-radial-menu
  offset = (Math.PI / 2) - angle;
  //for loop for setting the position for all the buttons.
  for (var i = 0; i < buttons.length; i++) {

  //Current angle is decided based on the buttons position in the buttons array
  currentAngle = i * angle + offset;
  //These two lines force us to use radians... and calculate the position for the button
  currentPositionX = cursorRadius * Math.cos(currentAngle);
  currentPositionY = cursorRadius * Math.sin(currentAngle);
//Sets the buttons position with CSS style
  buttons[i].style.left = (e.clientX + currentPositionX) + "px"
  buttons[i].style.top = (e.clientY + currentPositionY) + "px"
    }
  }
}
//Function for buttonclicking, posts the innerHTML of the button that was clicked
function buttonClick(element) {
  //Temporary non output
  console.log("you clicked on the button: " + element.innerHTML);
//Disables the display (hides) all the buttons onclick
  for (var i = 0; i < buttons.length; i++) {
    buttons[i].style.display = "none";
  }
  //Hides the radial circle
  document.getElementById("radial").style.display = "none";
  //unlocks the menu so we can click everywhere again
  menuStatus = false;
}
//function for creating a button
function createButton(){
  //variables containing HTML elements
  const newButton = document.createElement("button");
  const newContent = document.createElement("SPAN");
  const spanContent = document.createTextNode(buttons.length + 1);
  //Adding content to those elements
  newContent.appendChild(spanContent);
  newContent.className +="btnSpan";
  newButton.className += "btn";
  newButton.setAttribute("onclick", "buttonClick(this)");
  newButton.appendChild(newContent);

//Adding the elements to the correct position in the HTML document
  const currentDiv = document.getElementById("button1");
  document.body.insertBefore(newButton, currentDiv);
  buttons.push(newButton);
  console.log("AAAAAAAAAAAA");

}
