
//All the variables
let rotX = 1;
let rotY = 1;
let rotSpeed = 1;
let moveSpeed = 500;
let ball;
let dx = 2;
let dy = -2;
let x;
let y;
let targetX;
let targetY;
let targetishX;
let targetishY;
let constraintX;
let contraintY;
let angle = 0;
//setup
function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(10);
  constraintX = width / 2;
  contraintY = height / 2;
//instantiate the ball for the throbber here
  ball = new Ball();
  targetX = random(constraintX);
  targetY = random(contraintY);
  console.log(targetX);
}


function draw() {
background(50,100,144,90);
translate(width / 2, height / 2)

let v0 = createVector(constraintX, contraintY);
let v1 = createVector(100, 0);

  drawArrow(v0, v1.rotate(angle), 'black');
   angle += 0.5;


x = x + dx;
y = y + dy;
//call targetdraw to make a new target
TargetDraw();
//Draw the ball
ball.Display();

// If statement to set a staticrotation speed to stop the ball from rotating further: TODO: make work
if(mouseIsPressed == true)
{

    ball.StaticrotSpeed = rotSpeed;
    console.log(ball.StaticrotSpeed);
}
else {
  //ball.yeetStatus = false;
  rotSpeed = rotSpeed + 0.01;
}
  ball.moveX += dx;
  ball.moveY += dy;

}
// TODO: turn this interactive
function drawArrow(base, vec, myColor) {
  push();
  stroke(myColor);
  strokeWeight(3);
  fill(myColor);

  line(0, 0, vec.x, vec.y);
  rotate(vec.heading());
  let arrowSize = 7;

  ellipse(vec.mag() - arrowSize,0,50,50);
  pop();
}

/// Ball class
class Ball
{
  constructor()
  {
    this.x = width / 2;
    this.y  = height / 2;
    this.moveX = this.x;
    this.moveY = this.y;
    this.StaticrotSpeed = 0;
    this.yeetStatus = false
    this.position = createVector(this.x,this.y);
    this.heading = this.position.heading();

  }
  // Method for displaying and running the balls movement (TODO: Update name)
  Display()
  {

// TODO: Try and use/learn vectors
    push()

    //here be movement matey
    if(mouseIsPressed || this.yeetStatus == true){
      push()
      rotate(this.StaticrotSpeed);
      translate(this.moveX,this.moveY);
      this.yeetStatus = true;
      if(this.moveX >= targetX && this.moveX <= (targetX + 100) && this.moveY >= targetY && this.moveY <= (targetY + 100))
      {
        console.log("YOU WIN");
        this.yeetStatus == false;
      }
    }
    //base throbber position
    else{

    rotate(degrees(rotSpeed));
    //this.position.rotate(degrees(rotSpeed));
    this.moveX = this.x;
    this.moveY = this.y;
  }
      // the ball is drawn/displayed here
      ellipse(this.position.x,this.position.y,40,40);
      pop();
      console.log(this.heading);

  }


}

//Function to draw the target TODO: update so something happens on target collision
function TargetDraw()
{

    rect(targetX,targetY,100,100);
    targetishX = targetX
}

function windowResized()
{
  resizeCanvas(windowWidth, windowHeight);
}
