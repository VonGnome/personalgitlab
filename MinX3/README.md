# MiniX3: Throbberball
Welcome to my miniX3 product README, here I go over my product and some of the cultural significance behind loading.

[![The product](https://i.gyazo.com/418b8745aa4f9b957d7ca04a3311beb7.gif)](https://gyazo.com/418b8745aa4f9b957d7ca04a3311beb7)
## the linkydoodledoos
[The Product](https://vongnome.gitlab.io/personalgitlab/MinX3/)  
[The code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX3/Core.js)

## What I have made
What I have made (or tried to make depending on my motivation) is a small game using the throbber as a ball thrower, using the players knowledge and experience with ball games IRL, and
the throbbers ball like features to signify the fact that, this is indeed a game. The game is played by clicking a button, which "throws" the throbber out of its infinite loop
and sends it out towards the location it was going. The goal of the game is hitting the goal rectangle and getting as many points as possible.

## Critical thinking is loading, please wait...
Like so many other elements in any user interfaces (but especially GUIs) the throbber and its loading icon cousins serve as a way to create a more palatable interface to the complexities of the world and complexities of the computer. Because instead of seeing the lines of code working in the background the loading gives the user a single bar or ball to follow as it moves across the screen in a predictable, yet always surprising manner. The computer works on a clear and relatively precise timeschedule and time scale, but when translated through throbbers and loading bars without any numbers, the user get to enjoy the infinite feeling of waiting for something without a clear idea of how long they will be waiting for, or in some cases what they are waiting for, thus the throbber makes the user become more comfortable with the infinite wait time, which if something goes wrong behind the screen means that they can sit there for a while without knowing whether there is a problem, or if the software is working as intended. 


# Sources
Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
[Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98](https://monoskop.org/log/?p=20190) (2018) .
[Daniel Shiffman, Code! Programming with p5.js on YouTube, The Coding Train. Practical usage on conditional statements, loops, functions and arrays](https://www.youtube.com/watch?v=1Osb_iGDdjk) - Specifically 3.1, 3.2, 3.3, 3.4, 4.1, 4.2, 5.1, 5.2, 5.3, 7.1, 7.2)

