//for anyone reading this code, I apologise its proper cursed due to many rewrites
//Initializing variables
let mouseMove = 300;// essential variable for mouth movement
let mouthMover = 300;// essential variable for mouth movement
let happyPoints;
let sadPoints;
let happyPointDiv;
let sadPointsDiv;
let happy;

//this is the setup run on start
function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(60);
//Creating the UI in setup insures that its there for use in Draw()
happyPoints = 0;
sadPoints = 0;
  happy  = createDiv('Make Bob happy');
  happy.position(790,500);
  happy.size(500, AUTO);
  happy.style('font-size' ,'50px' );
  happyPointDiv = createDiv("Bob has been happy for: " + happyPoints + " frames");
    happyPointDiv.position(790, 580);
    happyPointDiv.size(500, AUTO);
    happyPointDiv.style('font-size' ,'25px')
    sadPointDiv = createDiv("Bob has been sad for: " + sadPoints + " frames");
      sadPointDiv.position(790, 620);
      sadPointDiv.size(500, AUTO);
      sadPointDiv.style('font-size' ,'25px')
background(50,100,144);

}





function draw() {
background(50,100,144);
//Updates and clears changes to UI elements
happy.html("Make Bob happy");
happy.style('color', '#000000')
happyPointDiv.html("Bob has been happy for: " + happyPoints + " frames")
sadPointDiv.html("Bob has been sad for: " + sadPoints + " frames")
  push(); // this probably isnt needed, but I am scared to touch and dont think it hurts
  noFill();
  if (mouseIsPressed == false) // If statement for anything that happens when Bob is sad
  {
    //point giver if statement
    if( mouthMover <= 340)
    {
      sadPoints = sadPoints + 1;
    }
    //If statements for moving the mouth upwards based on its current location and amount of sadpoints
    if(sadPoints >= 300 && mouthMover >= 300)
    {
      mouthMover = mouthMover - 3;
      mouthMover = mouthMover - 1;

    }
    else if (mouthMover >= 300)
    {
      mouthMover = mouthMover - 1;
    }
    if(sadPoints >= 600 && mouthMover <= 340 && mouthMover >=200)
    {
      mouthMover = mouthMover - 2;
    }
    //mouth
    beginShape()
    strokeWeight(4)
    vertex(900, 370)
    bezierVertex(900, mouthMover, 1000, mouthMover, 1000, 370)
    endShape()

  }
  if(mouseIsPressed) //Bob happiness code block
  {
     mouseMove = floor(map(mouseY, 0, 100, 0, 100));
   //mouth
  beginShape()
  strokeWeight(4)
  vertex(900, 370)
  bezierVertex(900, mouseMove, 1000, mouseMove, 1000, 370)
  endShape()
  mouthMover = mouseMove;
  //point giver if statement
  if(mouseMove > 379 && mouseMove < 460){
    happyPoints = happyPoints + 1;
  }
  }
  //Drawing rest of face here could probably be done in setup, but again afraid to touch. (or in its own function)
  pop();
  push();
  noFill();
  strokeWeight(4);
  ellipse(950,315,220,280);
  pop();
  ellipse(900,250,25,25);
  ellipse(1000,250,25,25);
  fill('green');
  if(mouthMover >= 460)
  {
    let red = 181
    let colu = color(red,3,7);
    happy.html("BOB IS TOO HAPPY!");
    happy.style('color', colu);


  }
}
