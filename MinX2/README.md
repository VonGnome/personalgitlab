# Sad and happy emojis: with Bob the not builder
[![Image from Gyazo](https://i.gyazo.com/c8bec4edaaeeb91fdeb4939cb0b4889d.gif)](https://gyazo.com/c8bec4edaaeeb91fdeb4939cb0b4889d)

## The links: 
[The product](https://vongnome.gitlab.io/personalgitlab/MinX2/)

[The code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX2/Core.js)

## Wot I made:
On this weeks rendition of the miniX I decided to create a gamification of the happy :) and sad :( emoticons to minimize my need for artistic expression using code and maximise time spent working on interactivty and temporality, as in my opinion, those two elements are the pillar advantages code art has over traditional graphical design. Thus the creation of Bob, a "human" the player is tasked to "make happy" By not telling the player how to and letting the player found out on their own, the game better affords introspection and thought about *what* can make Bob and by extension other people happy. The game also incentivises keeping Bob happy by giving points to the player, thereby affording competetion between players to keep Bob happy the longest. Conversely, they could also compete in keeping Bob sad, but because making Bob sad doesnt require interaction, but instead simply letting the game play itself, this action is disentivised via the want to interact. The player is also prompted to think about their own ways to achieve happiness, as any attempt to cheat the game, leads Bob to become sad, and if Bob is left sad for too long, Bob will become sad easilier.

[//]:  # "why would I use code to create vector art, instead of oh I dont know a piece of software dedicated to the creation of it, if not to add interactivity? tsk tsk..."

### code level:
On the code level this is all achieved by a simple bezier vector, where the y coordinate of its two control points is decided by a variable. if the mouse is clicked, the variable is the same as the mouses y position `if(mouseIsPressed){mouseMove = mouseY ...}` this controls the "happy" state of Bob, and in other words the mouse and consequently player controls Bobs mouth, but when the player isnt pressing the mouse its instead controlled by a different variable `mouthMover` that is equal to `mouseMove - 1` untill the mouth is back to its original sad state, so in code this looks like: `if(mouthMover >= mouthSadPos){mouthMover = mouthMover- 1 } ...` here mouthMovers value has been set to equal mouseMove in a previous frame wherein the `(mouseIsPressed)` code block has been run.

## oh right this was about emojis
While working on Bob i noticed something about how we draw happiness with emoticons. When destinguishing between "happy" and "really happy" we usually add an extra line in the form of a D :) :D and the same goes for when drawing simple smileys, I mention this as when dragging Bobs mouth around, if dragged to low to signify happiness it very quickly becomes *uncanny* where as, it can be dragged further up (signifying greater sadness) before the same problem is encountered. Thus it could be argued that it is easier to draw and form sadness from simple geometry compared to happiness, relatedly there are more ways to express destinct types of sadness through emotes fx ;_; for crying, than there are ways to express happiness. This could have an effect on how accurate we can be regarding our true emotional state, in the use of emotes. Where we can represent our sadness with ease through emotes, truly representing our happiness requires more expression and a larger variety of artwork. This leading to more attributes being assigned the simpel :) compared to the equally simple 
:(. Thus even in the most universal of emotes, a "problem" of representation can be found.

# Sources
 Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016.
[p5.js. p5.js | Simple Shapes.](https://p5js.org/examples/hello-p5-simple-shapes.html) .
[Daniel Shiffman, Code! Programming with p5.js, The Coding Train](https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2). [ Episode: 1.3,1.4,2.1,2.2]
