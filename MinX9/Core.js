//Api variables for use with giphys api and genderize.io
let urlgif = "https://api.giphy.com/v1/gifs/search"
let urlGender = "https://api.genderize.io?name="
let limit = "28"
let queery = "Male"
let apikey = "vFDSgQlbJ1xuswAJ4rVNwMGjbG6NcwZF";
let offset = "";
let name = "";
let nameInput;
let gendermebutton;
let genderequest = "";
let giphyRequest = "";

//variables not used directly for the api stuff
let drawingList = [];
let represent = false;
let restart = false;
let drawnAlready = false;
function setup(){
createCanvas(windowWidth,windowHeight);
//Setting the framerate to avoid epilepsy
frameRate(10);

background(221, 160, 221)

nameInput = createInput('write your name here'); //Name input DOM element creation
// text styling
textAlign(CENTER);
 fill(0, 102, 51);
 textSize(25);
text("Write in your name, and let the api assume you gender when ready", width / 2 + 50 ,(height/2) - 25);  //text creation
// nameinput styling
nameInput.position(width / 2, (height/2));
nameInput.input(myInput)
//Genderme button for starting the api call process
gendermebutton = createButton('assume my gender')
gendermebutton.position(width / 2, (height/2) + 50 )
gendermebutton.mousePressed(genderme);

}


function draw(){
  let col = color(random(255),random(255),random(255)); //Epilepsy machine or pride colours, the framerate decides
  if (represent == true) { //represent is found at the end of the api call in the genderme() function
   //Create the dom elements shown and used while the program is running
    let div = createDiv('Do you feel represented?');
    div.style('font-size', '50px');
    div.style('color', col);
    div.position((width / 2 - 100), (height / 2));
    let yesButton = createButton('Yes');
    yesButton.position((width / 2 - 100), (height / 2) + 50)
    yesButton.size(100,100);
    yesButton.mousePressed(yesScript)
    let noButton = createButton('No')
    noButton.position((width / 2) + 250, (height / 2) + 50)
    noButton.size(100,100);
    noButton.mousePressed(noScript)

  }
  //If statement for resetting back to the setup state of the program
  if (restart){
    //IMPORTANT! Constantly drawn input fields dont work, thats why we do this
    if (!drawnAlready){

    nameInput = createInput('Try anothers name here'); // small change to encourage new interaction with the program
    drawnAlready = true; //Found at the end of the api call
  }
  //DOM element styling
    nameInput.position(width / 2, (height/2));
    nameInput.input(myInput)
    gendermebutton = createButton('assume my gender')
    gendermebutton.position(width / 2, (height/2) + 50 )
    gendermebutton.mousePressed(genderme);
  }
}
// function for call on no button
function noScript(){
  //removes all the images that form the backgrond and calls genderme again
  let images = selectAll('img')
  for (var i = 0; i < images.length; i++) {
    images[i].remove();
  }
  genderme();
}
//function for call on yes button click
function yesScript(){
//Resets the program
  represent = false;
  restart = true;
  drawnAlready = false;
  removeElements();
}
//Function that contains the program
function genderme(){
restart = false
//positions for image locations
let xPos = 0;
let yPos = 0;
  //loads the JSON file from genderize.io and starts the chain of api calls when finished.
  loadJSON(genderequest, function(assumedgender){
    console.log(assumedgender.gender);
    //sets the search query used with the giphy api to the genderize' guessed gender
    queery = assumedgender.gender; // leaving these here for potential archeological inquiry
    // Full URL for the giphy api call
    giphyRequest = urlgif + "?q=" + queery + "&limit=" + limit + "&offset:" + int(offset) + "&api_key=" + apikey;
    console.log(giphyRequest); // leaving these here for potential archeological inquiry
    // Calls the giphy api for a search of 29 gifs. and then creates a list of those, finds the images(gifs) within and then draws them in a grid like formation
    loadJSON(giphyRequest, function(giflist){
      //Loops through the entire data list in the JSON file
      for (var i = 0; i < giflist.data.length; i++) {

        var gif // holder of the images
        gif = createImg(giflist.data[i].images.original.url, function(){
          // exit if statement for variables needed outside the callback
          if (i >= 24) {
          represent = true;
          offset += i; //used for the no fuction, so that new images should be loaded
          }
        })
        //sets size and pos of the images
        gif.size(250,250);
        gif.position(xPos,yPos);

        //Creates the grid
        xPos = xPos + 250;
        if(xPos >= width) {
        yPos = yPos + 250;
        xPos = 0;
        }
      }
    })
  }) //this is the end of the callback, yes all this is in a callback


}
//Function run when the input element is used
function myInput(){
  name = this.value();
  genderequest = urlGender + name + "&country_id=DK"; //genderize api call is set for use in genderme();
}
