# MiniX9: representation.gif
<img src="https://i.gyazo.com/b4c238f387f3ac21f5d49a9daa48a665.mp4
" width="480" height="240"/>

## Links
[The program](https://vongnome.gitlab.io/personalgitlab/MinX9/)

[The code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX9/Core.js)

### small note
This program uses the topic of representation as a way to engage with the topic of apis, and as that was the main topic of this weeks miniX the readME is mainly focused on that topic. If you feel uncomfortable with flashing lights, or this topic is especially close to home, the readME might still offer some insight without the need to use the program. [And if you feel for it you can engage in some hyperlink reading and get the question that sparked this program](https://gitlab.com/sofienisted/aesthetic-programming/-/issues/7)

## The program and apis it relies on
– What is the program about? Which API have you used and why?
We have made and program, which aims to collect a range of gifs from [GIPHY](https://giphy.com/) on the basis of the assumed gender given by the [genderize.io](https://genderize.io/) api based on the name input that the user makes. The name that is written in the text field is differentiated in gender; Man, woman or ```null```, respectively. From here, program makes a call to the giphy API and selects a collection of GIPHY to represent the assigned gender. Furthermore the user then has the option to declare themselves represented by these selected GIPHY or not. If yes, they will be returned to the starting point of the program. and be prompted to try with anothers name. And if not, the GIPHY will load again.

## The nature of api data

– **Can you describe and reflect on your process in this miniX in terms of acquiring,
processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data? What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?**

In the beginning we had a hard time figuring out, what we wanted to make, knowing that there are so many different API’s out there. In some way it is therefore hard to choose, because of all the opportunities that is available. After looking through some different API’s, we figured that we wanted to create something, that had some kind of relevans in todays world. In the beginning we wanted to create something based on Youtube's data API, which is owned by Google. But since it was too difficult to retrieve this data from Google, we went with an API from GIPHY. Which ended up being in relation to gender. Furthermore we wanted to create some kind of personal art, so that the user felt like they had an influence on what the program generated on the basis of their given data. Which was represented in pictures since we felt like, that would aesthetically pleasing, while we also think pictures say more than a thousand words.

Big companies like Google have many restrictions in their terms and conditions when using and accessing their APIs. This shows the power that some of the big companies’ API’s have. Because APIs are perceived as the glue for different platforms in order to be able to connect with each other and this need is something we see in almost every platform. Big companies as Google gains a lot of power through their APIs.  

"Similarly, in an API’s decision in regards to which components can and cannot be surfaced and shared (whether for security, business and/or practical reasons), an API can be seen as both an entry point into the black box of a particular computational service, but also as a clearly demarcated barrier towards other possible exchanges with this service."[(Soon,Snodgrass,2019)](https://journals.uic.edu/ojs/index.php/fm/article/view/9553/7721)

This quote also highlights the power-relationship between the us users and the big companies. In this case Google. They have decided which opportunities the user should have and, in their considerations, they are also aware of, what they want from the user.


APIs makes it possible for third parties to use the data from a given platform, just like we have used GIPHY made by other people. The data, in our case gifs, can be used without the acceptance from the person who created the data. In our example this would be that somebody uploads a gif on GIPHY and then we take the advantage of that and use it on our website without notifying this person or giving credit.

In our miniX9 we have used the raw data from the API, for everyone to see, if they want to. The gifs we could acquire from the site or not processed. But we have selected to focus on only a handful of the categories inside their vast collection of GIPHYS.

One way that platform providers controls their data, is through APIs keys. Where they can see how much data you retrieved and make you stop if you’ve exceeded your limit. They can also see how and what data you are using exactly. Google for example will allow you to request up to a hundred pictures a day from their picture API, but when you have reached your limit, you simply can’t request anymore. This is one of the power-relations there is at stake.  


## Question for the future
For the future the development, and regulation of the internet apis stand out as one of the most essential building blocks in the www, or more aptly the glue that holds the whole thing together as a piece of infrastructure not controlled directly by any goverment or state power (unless created by one ofcourse). This leads to the interesting question of *how* we should read apis in the future. Should they, be read and understood as critical infrastructure like highways and roads are for the western world today, and as such, should it be regulated, controlled and treated as such. Or should it become a new type of infrastructure, free of government oversight, and simply controlled by everchanging and oh so stale winds of capital?

## Giphy: the colonial business model
The question about data and worth is interesting, especially with our choice giphy. Giphys entire business model is based on its api, as the allure of the service is its integratability with other apps. The Giphy service is essence not as useful without our use of it, it is through colonization that the app gains value, we and everyone else using Giphys API, exchange a part of our freedom and autonomy in order to gain access to the simple goods that Giphy plentifully provides. This leaves one to wonder, what happens when all have given away their autonomy, and giphy goes down.

# Sources
* Soon Winnie & Cox, Geoff, "Que(e)ry data", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 186-210
* [Daniel Shiffman, “Working with data - p5.js Tutorial,” The Coding Train (10.1, 10.4 - 10.10)](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r).
* Eric Snodgrass and Winnie Soon, “API practices and paradigms: Exploring theprotocological parameters of APIs as key facilitators of sociotechnical forms of exchange],”First Monday 24, no.2 (2019), https://journals.uic.edu/ojs/index.php/fm/article/view/9553/7721.