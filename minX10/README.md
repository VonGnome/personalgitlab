# MiniX10: flowcharting away
*Made by group 10*

## Abstraction in the face of complexity
One of the disadvantages of a very simplistic flowchart may be that some of the information is lost, but if they are too complex, they become unreadable to the common person. The best way to combat this complexity is to choose your target audience and stick to it, for our flowcharts the point was to give an idea of the flow that our two ideas would have, on a level where the actual program wasn't set in stone yet, meaning they had to be flexible. So in our case it isn't a problem, that the flowcharts don't show anything about the code level, as that isn't its job/function.


### make me non binary: simplicity warning

![The flowchart](https://i.imgur.com/nE4kRyc.png)

This program doesn't have any large technical challenges, which sounds like a good thing but... Due to the simplistic nature of the program, it has the danger of being to small in scope, and with that it also cant be scaled up with ease. Beyond that the flowcharts abstraction also creates a concern of the program being harder than assumed, which the simplicity of the flowchart could be a signifier for.

### Freedom of speech project: avoiding a yikes/10
![the flowchart2](https://i.imgur.com/1YOqkGa.png)

This project faces the technical problem of being largely technically undefined, and what seems like simple tasks in the flowchart like for example: "downloading and reading the data" could quickly balloon out of control. Beyond that the program faces a non technical issue, as we have to walk on metaphorical eggshells since the in order to show the issues we wanna bring light to, we plan on giving a platform to bigots, so if we don't do it responsibly, it could end up platforming ideas we don't want to platform. We also don't want persons who could find offense with the content to miss out on our larger point, which is why we have the little jumping off point before the program gets started.  


### flowcharts as tools
Flowcharts help to give an overall impression of what ideas and thoughts, we have made about what the final project should be like. These two flowcharts are made as preparation for our final program, whereas our individual flowcharts are a way to explain our previously made programs in smaller chunks.

> "By the early 1970s, the conventional wisdom was that “developing a program flowchart is a necessary first step in the preparation of a computer program." (Ensmenger, 2016, p. 323).

As Ensmenger describes in his text on flowcharts and their use, the initial idea was that they should be used as a planning tool for the development of computer programs.
In our experience, flowcharts that are made before a program is written often have a higher level of abstraction, as they are made to describe the "flow" of the program, more than the actual functionality. This might be the other way around for flowcharts that are made after a program is written, as it, for the most part, is a lot easier and straightforward to describe the in-depth functionality of the code, thus leading to a lower level of abstraction but a more accurate illustration of what the flow of commands within the program.

## on the personal
*Made by Markus L. Elvig*
![The flowchart](https://i.imgur.com/M6p6JV9.png)
### for whom
The target audience for any flowchart, is one of the most important parts of creating a flowchart, because it controls the language and abstraction that the flowchart engages in. The flowcharts I have created over my miniX8 is meant for others in the design team with relative knowledge of code. I chose this because its the style I am used to, and because it creates an interesting flowchart for discussing what is abstracted away. In this style of flowchart, the powerholders are those most techsavvy, as they have the capability for imagination(source) beyond the abstracted technical level. this balance of power will of course, bring more technincal solutions rather than designer focused ones, and there is a larger risk of feature bloat.

### the abstracted
As seen in the flowchart, it consists of 2 flowcharts and a class diagram. This is because in order to *truly* show the complexity of the program in a simple manner each part must be abstracted from each other, and if only one is shown, important details will be lost. Yet, on their own, all of the charts work to show a part of the programs "flow". The flowchart on the left shows the main draw function of the program, here the complexities of the called functions are hidden behind simple plain english descriptions of what happens as an output. It is also here we find the reference to the person class which is described in a class chart. This way of drawing classes, give great insight into what the class consists of, but not how it functions, (in more filled out diagrams, the inheritance of the class would also be shown giving some clue to this) for this we need **another** flowchart. The final flowchart to the right, gives insight into the way the statementMaker() grammar checks its statements.

#### the statementMaker  
This is the most interesting (flowchart wise) part of of the miniX8 as it shows one of the few times it really could have payed off to make a flowchart. The flowchart shows how the program tries to check the grammar of the expletive sentences and add the ending that is needed. When we as humans check our grammar we quickly go through a similar thought process of "checking" our grammar, but we humans can quickly pick the right option without the need to check the one before. Since the program cant do this it needs to check the options one after another, since certain grammatical rules overlap it is important that the order it is checked in is correct. The program fails in this regard, as it doesnt always add the ending to the expletive, we could have probably helped ourselves a headache, if we had used a flowchart.

##### References
* [1] Soon, Winnie & Cox, Geoff, "Algorithmic procedures", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 211-226.
* [2] Ensmenger, Nathan, “The Multiple Meanings of a Flowchart,” Information & Culture: A Journal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013.
