let capture;
let mic;
let angle = 0;
let maxAngle = 1.3;
let minAngle;
let moveLeft;
let rH;
let rL;
let timer = 0;
let longestTime = 0;
let score;
let highScore;
let prevtimer = 0;
let playerPos = 500;
let button;
let gameState = false;

function setup()
{
  createCanvas(windowWidth, windowHeight);


    //setup face tracker
    mic = new p5.AudioIn();
    mic.start();
    minAngle = maxAngle * -1;
    moveLeft = width;
    longestTime = getItem('LongestTme');
    rH = random(25,300);
    rL = random((rH += 50),(rH += 100));
     score = createDiv("Time since you last hit a wall:" + floor(timer));
    highScore = createDiv("Longest time alive:" + longestTime);
    button = createButton('Start Game');
    button.position(width / 2, height / 2);
    button.mousePressed(StartGame);
}



function draw() {
  background(75);

if(!focused)
{
  button.show();
  gameState = false;
}

if(gameState == true){

timer = ((millis() - prevtimer) / 1000);
score.html("Time since you last hit a wall:" + floor(timer));
highScore.html("Longest time alive:" + floor(longestTime));

score.position(50,50);
highScore.position(50,100);

  if(moveLeft == 0)
  {
    rH = random(25,300);
    rL = random((rH += 50),(rH += 100));
    moveLeft = width + 10;
  }

  moveLeft = moveLeft -= 10;
    let characterPos
    let vol = mic.getLevel();

    let v0 = createVector(500, 400);
      let v1 = createVector(100, 350);

drawArrow(v0, v1.rotate(angle), 'black');

if(vol >= 0.0009 && v1.heading() >= minAngle)
{

  angle = angle - vol;

}
else if (v1.heading() <= maxAngle)
{
  angle += 0.01
}

characterPos = 400 + v1.y;
 ellipse(playerPos,characterPos,40,40);

 drawObstacle(moveLeft,rH,rL);
if(moveLeft == playerPos)
{
  console.log(characterPos);
  console.log(rL);
  console.log(rH);
  if(characterPos <= rH)
  {
    if(timer > longestTime){
      longestTime = timer;
      storeItem('LongestTme', longestTime)
    }
      prevtimer = millis();
      console.log("HIT HIGH");
      console.log(rH);
  }
  else if (characterPos >= rL  && characterPos >= (rH + rL))
  {
    prevtimer = millis();
    console.log("HIT LOWER");
    if(timer > longestTime){
      longestTime = timer;
      storeItem('LongestTme', longestTime)
    }

  }
}

}
}
// draw an arrow for a vector at a given base position
function drawArrow(base, vec, myColor) {
  push();

  translate(base.x, base.y);
  //line(0, 0, vec.x, vec.y);
  rotate(vec.heading());
  let arrowSize = 7;
  translate(vec.mag() - arrowSize, 0)  ;
  //triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
  pop();
}

function StartGame()
{
  gameState = true;
  button.hide();
}

function drawObstacle(posx,lengthH,lengthL)
{
  //Top obstacle
  rect(posx,1,30,lengthH);
  //Lower Obstacle
  rect(posx,(lengthH + lengthL),30, height);

  }
