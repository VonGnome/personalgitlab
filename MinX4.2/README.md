
# MiniX4: Through pain and suffering: A flappy sound bird
This is my work for the 4th miniX
[![Image from Gyazo](https://i.gyazo.com/f8e346a8bb5b0985b9e48fab35871587.png)](https://gyazo.com/f8e346a8bb5b0985b9e48fab35871587)
## the linkydoodledoos
[The Product](https://vongnome.gitlab.io/personalgitlab/MinX4.2/)  
[The code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX4.2/Core.js)

## What is this
What I have created for this miniX is a simplistic version of the "hit game" from long ago flappy bird, where in the goal is to get as far as possible, but with a twist. instead of using your touch or mouse click input the game uses the volume of your voice for its controls. By making this simple change in the core gameplays design, the games difficulty and nature changes completely, as the control schemes affordances encourage experimenting with what type of sound production creates the most control over the characters movement, aswell as encouraging staying calm during gameplay, as raising your voice or slamming the table could be read as input. This is also reinforced by the games neverending nature, as there is never a pause in when you are being recorded. This nature of always being recorded is something inherent with sound activated controls in p5.js' sound library, as the program cant differentiate between different sounds but only their volume levels. And like all sound based controls it is always listening for the next input, but compared to a keyboard click the player/user can have a harder time creating a specific mental model of **when** they are inputting and when they arent. So when this difference is gamified and put front and center in a simplistic game, it becomes more obvious for the player, and hopefully causes the player to relate this new found knowledge to other artefacts in their life.

# Sources
Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
[“p5.js examples - Interactivity 1,”](https://p5js.org/examples/hello-p5-interactivity-1.html).
[“p5.js examples - Interactivity 2,”](https://p5js.org/examples/hello-p5-interactivity-2.html).
[“p5 DOM reference,”](https://p5js.org/reference/#group-DOM).
[Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary”](https://youtu.be/hIXhnWUmMvw).
[Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4, 2019. Web. 16 Feb. 2021.](https://policyreview.info/concepts/datafication)

