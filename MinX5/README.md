# MiniX5: Bots making passwords
[![Image from Gyazo](https://i.gyazo.com/3cd65e7b6153255a7e9bb3322a95dc11.gif)](https://gyazo.com/3cd65e7b6153255a7e9bb3322a95dc11)
## the l1nk5
[The product](https://vongnome.gitlab.io/personalgitlab/MinX5/)

[If you cant read bot speech](http://www.robertecker.com/hp/research/leet-converter.php?lang=en)
 
[the code](https://gitlab.com/VonGnome/personalgitlab/-/blob/master/MinX5/Core.js)

## Description of the program
What is created or attempted, is a simulation of two bots, made in the form of functions, the makeid function creating the bots password using pseudo randomness, from here on in I will let the bots take over writing, they only understand code and leetspeak, sorry in advance.
```
function makeid(length) {
   var botPassword = '';
   var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      botPassword += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return botPassword;
} 
```
4nd 7h3 6u3553r b07, 7h47 6u35535 p455w0rd5, u51n6 bru73 f0rc3 (c0mp4r3d 70 4 5p3c1f1c 50r71n6 4l60ry7hm)
```
//No longer broken
function GuesserBot(password){

  let interations = 0;
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var arr = Array.from(password)

    console.log(arr);
    console.log(arr[0]);
    //for loop for guessing the entire password

      let guessedPassword = [];
    for(var i = 0; i < arr.length; i++)
    {
      //Checks through characters if array is eqaul to character is the first string in the pass e? if not is it e...
      for(var j = 0; j < characters.length; j++){
      pointer = characters.charAt(j);
      console.log(pointer);

      //returns usable guesses
      if ( pointer == arr[i])
      {

        guessedPassword.push(pointer);

      }
      else interations++;

    }

    }
    guessedPassword.toString();
    passwordCracked = true;
    return ("The hacker bot guessed the password:" + guessedPassword + " after:" + interations + " iterations");


}
```
7h353 7w0 func710n5 4c7 1n 4 c0n574n7 57ru66l3 wh3r3 0n3 m4k35 4 n3w p455w0rd, 4nd 7h3 07h3r 6u35535 175 c0n73n75 w17h1n m1l153c0nd5, 0nly f0r 7h3 07h3r 70 7ry 4641n 4nd 50 0n 4nd 50 f0r7h. 7h3 m4k31d b07 w0rk5 0f 7h3 rul3 7h47 f0r 7h3 l3n6h7 0f 7h3 w15h3d p455w0rd, pl4c3 4 r4nd0mly p1ck3d ch4r4c73r. 4nd 7h3 6u3553r b07 w0rk5 by u51n6 1 51mpl3 rul3: f0r 3v3ry ch4r4c73r 1n 7h3 p455w0rd, 7h3 b07 "7h1nk5 l1k3 50 60 70 7h3 f1r57 ch4r4c73r 1n 7h3 p455w0rd: 4k5 7h3 qu35710n "15 17 3qu4l 70 7h3 f1r57 ch4r4c73r 1n 7h3 ch4r4c73r5 v4r14bl3"? (1n 3553nc3 7h3 b075 4lph4b371c4l kn0wl3d63 b4nk) 1f 17 15, pu7 7h47 1n70 7h3 6u3553d p455w0rd 4nd m0v3 0n 70 7h3 n3x7 ch4r4c73r 1n 7h3 p455w0rd 4nd 45k 4641n, 1f 17 15n7, 17 7h3n 45k5 "15 17 3qu4l 70 7h3 5c0nd ch4r4c73r 1n 7h3 ch4r4c73r5 v4r14bl3?". 7h3 b07 d035 7h15 un71ll 7h3 4n5w3r 15 7h3 54m3 45 7h3 0r161n4l p455w0rd 17 w45 61v3n. 17 7h3n r37urn5 17 plu5 50m3 3x7r4 fluff. 73ll1n6 7h3 07h3r b07 "h3y 1 607 y0u"


### 7h3 c0nc3p7u4l l34k463:
wh47 1 57r1v3d 70 3nl16h73n w45 7h3 c0n574n7 57ru66l3 w3 h4v3 0nl1n3 wh3n 7ry1n6 70 cr3473 p455w0rd5 f0r 0ur mul71pl3 5y573m5, 4nd 7h3 57ru66l3 0f 7ry1n6 70 k33p up w17h 3v3r m0r3 c0mpl1c473d b075 4nd l34k5 7h47 4r3 4bl3 70 6u355 7h3 k3y 70 0ur m057 v4lu4bl3 0f p053510n5, wh1l3 571ll b31n6 4bl3 70 r3m3mb3r 4 numb3r 0f p455w0rd5. 4 m37h0d f0r k33p1n6 up, u53d by u5 hum4n5 15 7h3 50 c4ll3d p455w0rd 63n3r470r, 3553n714lly, 4 5173/4pp wh3r3 y0u 0nly n33d 0n3 k3y, 70 4cc355 r4nd0mly 63n3r473d k3y5. 4nd 45 w3 h4v3 l34rn3d 7hr0u6h 7h3 10 pr1n7 ch4p73r 0n 537 r4nd0mn355, 7h3 w0rd 4nd 4ppl1c4710n5 0f f0rm4l r4nd0mn355 15 35p3c14lly 1mp0r74n7 w17h1n 7h3 53cur17y 53c70r 0f 17, 45 17 15 7hr0u6h 63n3r471v3 b075 7h47 c0mp373 w17h 34ch07h3r 7h47 w3 hum4n b31n65 4r3 l3f7 b3h1nd 1n 7h3 du57, 70 0nly 53nd 0u7 3v3r 1ncr3451n6 c0mpl3x b075 70 h0p3fully cr4ck 1n70 7h3 5w337 n ju1cy d474b4535.

# Sources
Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
[Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10](https://10print.org/) (Cambridge, MA: MIT Press, 2012), 119-146.
[Daniel Shiffman, “p5.js - 2D Arrays in Javascript,” Youtube](https://www.youtube.com/watch?v=OTNpiLUSiB4).
